﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace GarmentFactory.Convertors
{
    public class ImageConvertor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var header =  value.ToString().ToLower();
            var uri = new Uri(string.Format(@"D:\Studying\labs\KPZ\lab5\GarmentFactory\GarmentFactory\Pictures\{0}.png", header), UriKind.Absolute);
            return new BitmapImage(uri);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
