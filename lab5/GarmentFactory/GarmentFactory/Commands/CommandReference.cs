﻿using System;
using System.Windows;
using System.Windows.Input;

namespace GarmentFactory.Commands
{
    public class CommandReference : Freezable, ICommand
    {
        // using ICommand:
        private Action<string> _execute;
        public CommandReference(Action<string> action)
        {
            _execute = action;
        }

        public CommandReference()
        {
            
        }

        //public static readonly DependencyProperty CommandProperty = DependencyProperty.Register("Command", typeof(ICommand), typeof(CommandReference), new PropertyMetadata(new PropertyChangedCallback(OnCommandChanged)));

        //public ICommand Command
        //{
        //    get { return (ICommand)GetValue(CommandProperty); }
        //    set { SetValue(CommandProperty, value); }
        //}

        #region ICommand Members

        public bool CanExecute(object parameter)
        {
            //if (Command != null)
            //    return Command.CanExecute(parameter);
            //return false;

            // using ICommand:
            if (_execute != null)
            {
                return true;
            }
            else return false;
        }

        public void Execute(object parameter)
        {
            //Command.Execute(parameter);
            
            // using ICommand:
            _execute.Invoke(parameter as string);
        }

        public event EventHandler CanExecuteChanged;

        private static void OnCommandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CommandReference commandReference = d as CommandReference;
            ICommand oldCommand = e.OldValue as ICommand;
            ICommand newCommand = e.NewValue as ICommand;

            if (oldCommand != null)
            {
                oldCommand.CanExecuteChanged -= commandReference.CanExecuteChanged;
            }
            if (newCommand != null)
            {
                newCommand.CanExecuteChanged += commandReference.CanExecuteChanged;
            }
        }

        protected override Freezable CreateInstanceCore()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
