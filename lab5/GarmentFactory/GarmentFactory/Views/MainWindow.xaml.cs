﻿using GarmentFactory.BusinessLogic;
using GarmentFactory.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GarmentFactory
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var product = (ProductViewModel) ProductsUserControl.dataGridProducts.SelectedItem;
            if (product != null || product is ProductViewModel) product.Quantity += 10;
        }

        private void Animation(object sender, MouseEventArgs e)
        {
            if (Rect.Margin.Bottom != 50 )
            {
                TranslateTransform translate = new TranslateTransform();
                Rect.RenderTransform = translate;
                DoubleAnimation animation = new DoubleAnimation(0, - MyMainWindow.Height, TimeSpan.FromSeconds(10));
                translate.BeginAnimation(TranslateTransform.YProperty, animation);
            }

        }
    }
}
