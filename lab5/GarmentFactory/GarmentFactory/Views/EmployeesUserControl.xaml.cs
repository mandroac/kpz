﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GarmentFactory.Views
{
    /// <summary>
    /// Interaction logic for EmployeesUserControl.xaml
    /// </summary>
    public partial class EmployeesUserControl : UserControl
    {


        public int ShowControl
        {
            get { return (int)GetValue(ShowControlProperty); }
            set { SetValue(ShowControlProperty, value); }
        }

            public static readonly DependencyProperty ShowControlProperty =
            DependencyProperty.Register("ShowControl", typeof(int), typeof(EmployeesUserControl), new PropertyMetadata(0));


        public EmployeesUserControl()
        {
            InitializeComponent();
        }
    }
}
