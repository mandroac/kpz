﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace GarmentFactory.BusinessLogic
{
    [DataContract]
    public class Product
    {
        private string _name;
        private string _description;
        private int _quantity;

        public Product()
        {
        }

        public Product(string name, string description, int quantity)
        {
            _name = name;
            _description = description;
            _quantity = quantity;
        }

        [DataMember]
        public string Name { get => _name; set => _name = value; }
        [DataMember] 
        public string Description { get => _description; set => _description = value; }
        [DataMember] 
        public int Quantity { get => _quantity; set => _quantity = value; }

    }
}
