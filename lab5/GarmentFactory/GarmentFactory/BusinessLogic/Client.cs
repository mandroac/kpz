﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace GarmentFactory.BusinessLogic
{
    [DataContract]
    public class Client
    {
        private string _name;
        private Product _product;
        private int _rating;

        public Client(string name, Product product, int rating)
        {
            Name = name;
            Product = product;
            Rating = rating;
        }
        [DataMember]
        public string Name { get => _name; set => _name = value; }
        [DataMember] 
        public int Rating { get => _rating; set => _rating = value; }
        [DataMember] 
        public Product Product { get => _product; set => _product = value; }
    }
}
