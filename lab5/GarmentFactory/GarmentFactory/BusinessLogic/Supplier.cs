﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace GarmentFactory.BusinessLogic
{
    [DataContract]
    public class Supplier
    {
        private string _name;
        private string _description;
        private bool _isActive;
        private DateTime _lastSupply;

        public Supplier(string name, string description, bool isActive, DateTime lastSupply)
        {
            _name = name;
            _description = description;
            _isActive = isActive;
            _lastSupply = lastSupply;
        }

        [DataMember] 
        public string Name { get => _name; set => _name = value; }
        [DataMember] 
        public string Description { get => _description; set => _description = value; }
        [DataMember] 
        public bool IsActive { get => _isActive; set => _isActive = value; }
        [DataMember] 
        public DateTime LastSupply { get => _lastSupply; set => _lastSupply = value; }
    }
}
