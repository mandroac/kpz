﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using GarmentFactory.Serialization;

namespace GarmentFactory.BusinessLogic
{
    [DataContract]
    public class DataModel
    {
        public static string DataPath = "organizer.dat";
        public DataModel()
        {
            Employees = new List<Employee> { new Employee("Джеймс Бонд", 15000.00, "Прибиральник", DateTime.Today) };
            Products = new List<Product> { new Product() { Name = "Штани 1", Description = "з начосом", Quantity = 100 }};
            Clients = new List<Client> { new Client("Staff", Products.ToList()[0], 150) }; 
            Suppliers = new List<Supplier> { new Supplier("Фабрика Весна", "fsddhf", true, DateTime.Today) };
        }

        [DataMember] 
        public IEnumerable<Client> Clients { get; set; }
        
        [DataMember] 
        public IEnumerable<Employee> Employees { get; set; }
        
        [DataMember] 
        public IEnumerable<Product> Products { get; set; }
        
        [DataMember] 
        public IEnumerable<Supplier> Suppliers { get; set; }

        public static DataModel Load()
        {
            if (File.Exists(DataPath)) return DataSerializer.Deserialize(DataPath);
            else return new DataModel();
        }

        public void Save()
        {
            DataSerializer.Serialize(DataPath, this);
        }
    }
}
