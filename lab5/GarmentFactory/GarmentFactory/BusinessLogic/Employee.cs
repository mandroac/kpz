﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace GarmentFactory.BusinessLogic
{
    [DataContract]
    public class Employee
    {
        private string _fullname;
        private double _salary;
        private string _position;
        private DateTime _employmentDate;

        public Employee(string fullname, double salary, string position, DateTime employmentDate)
        {
            _fullname = fullname;
            _salary = salary;
            _position = position;
            _employmentDate = employmentDate;
        }

        [DataMember]
        public string Fullname { get => _fullname; set => _fullname = value; }
        [DataMember] 
        public double Salary { get => _salary; set => _salary = value; }
        [DataMember] 
        public DateTime EmploymentDate { get => _employmentDate; set => _employmentDate = value; }
        [DataMember] 
        public string Position { get => _position; set => _position = value; }
    }
}
