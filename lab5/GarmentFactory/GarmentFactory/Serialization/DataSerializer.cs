﻿using GarmentFactory.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.IO;

namespace GarmentFactory.Serialization
{
    public class DataSerializer
    {
        public static void Serialize(string fileName, DataModel dataModel)
        {
            var formatter = new DataContractSerializer(typeof(DataModel));
            var s = new FileStream(fileName, FileMode.Create);
            formatter.WriteObject(s , dataModel);
            s.Close();
        }

        public static DataModel Deserialize(string fileName)
        {
            var s = new FileStream(fileName, FileMode.Open);
            var formatter = new DataContractSerializer(typeof(DataModel));
            return (DataModel)formatter.ReadObject(s);
        }
    }
}
