﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using AutoMapper.Mappers;
using GarmentFactory.BusinessLogic;
using GarmentFactory.ViewModels;

namespace GarmentFactory.Base
{
    public class Mapping
    {
        public void Create()
        {
            Mapper.CreateMap<DataModel, DataViewModel>();
            Mapper.CreateMap<DataViewModel, DataModel> ();

            Mapper.CreateMap<Client, ClientViewModel>();
            Mapper.CreateMap<ClientViewModel, Client>();

            Mapper.CreateMap<Employee, EmployeeViewModel>();
            Mapper.CreateMap<EmployeeViewModel, Employee>();

            Mapper.CreateMap<Product, ProductViewModel>();
            Mapper.CreateMap<ProductViewModel, Product>();

            Mapper.CreateMap<Supplier, SupplierViewModel>();
            Mapper.CreateMap<SupplierViewModel, Supplier>();

        }
    }
}
