﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GarmentFactory.ViewModels
{
    public class ProductViewModel : ViewModelBase
    {
        private string _name;
        private string _description;
        private int _quantity;


        public string Name { get => _name;
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }
        
        public string Description { 
            get => _description;
            set
            {
                _description = value;
                OnPropertyChanged("Description");
            }
        }
        public int Quantity { 
            get => _quantity;
            set
            {
                _quantity = value;
                OnPropertyChanged("Quantity");
            }
        }
    }
}
