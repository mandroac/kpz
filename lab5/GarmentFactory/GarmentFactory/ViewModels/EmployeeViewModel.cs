﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GarmentFactory.ViewModels
{
    public class EmployeeViewModel : ViewModelBase
    {
        private string _fullname;
        private double _salary;
        private string _position;
        private DateTime _employmentDate;

        public string Fullname { 
            get => _fullname;
            set
            {
                _fullname = value;
                OnPropertyChanged("Fullname");
            }
        }
        public double Salary { 
            get => _salary;
            set
            {
                _salary = value;
                OnPropertyChanged("Salary");
            }
        }
        public DateTime EmploymentDate { 
            get => _employmentDate;
            set
            {
                _employmentDate = value;
                OnPropertyChanged("EmploymentDate");
            }
        }
        public string Position { 
            get => _position;
            set
            {
                _position = value;
                OnPropertyChanged("Position");
            }
        }
    }
}
