﻿using GarmentFactory.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Text;

namespace GarmentFactory.ViewModels
{
    public class ClientViewModel : ViewModelBase
    {
        private string _name;
        private Product _product;
        private int _rating;

        public string Name { 
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }

        }
        public int Rating 
        { 
            get => _rating; 
            set 
            { 
                _rating = value;
                OnPropertyChanged("Rating");
            } 
        }
        public Product Product { 
            get => _product; 
            set 
            {
                _product = value;
                OnPropertyChanged("Product");
            }
        }
    }
}
