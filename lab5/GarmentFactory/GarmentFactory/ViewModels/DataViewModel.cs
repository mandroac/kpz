﻿using GarmentFactory.BusinessLogic;
using GarmentFactory.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;

namespace GarmentFactory.ViewModels
{
    public class DataViewModel : ViewModelBase
    {
        private ObservableCollection<ClientViewModel> _clients;
        private ObservableCollection<EmployeeViewModel> _employees;
        private ObservableCollection<ProductViewModel> _products;
        private ObservableCollection<SupplierViewModel> _suppliers;
        private string _visibleControl = ProductsType;

        public static string EmployeesType { get => "Employees"; }
        public static string ProductsType { get => "Products"; }
        public static string SuppliersType { get => "Suppliers"; }

        //using ICommand:
        public ICommand SetControlVisibility { get; set; }

        public DataViewModel()
        {
            //using ICommand:
            SetControlVisibility = new CommandReference(ControlVisibility);
        }

        // using ICommand:
        public void ControlVisibility(object args)
        {
            VisibleControl = args.ToString();
        }

        public ObservableCollection<ClientViewModel> Clients { 
            get => _clients;
            set
            {
                _clients = value;
                OnPropertyChanged("Clients");
            } 
        }

        public ObservableCollection<EmployeeViewModel> Employees { 
            get => _employees; 
            set
            {
                _employees = value;
                OnPropertyChanged("Employees");
            }
        }

        public ObservableCollection<ProductViewModel> Products { 
            get => _products; set
            {
                _products = value;
                OnPropertyChanged("Products");
            }
        }

        public ObservableCollection<SupplierViewModel> Suppliers { 
            get => _suppliers; 
            set
            {
                _suppliers = value;
                OnPropertyChanged("Suppliers");
            }
        }

        public string VisibleControl { 
            get => _visibleControl;
            set 
            { 
                _visibleControl = value;
                OnPropertyChanged("VisibleControl");

            }
        }
    }
}
