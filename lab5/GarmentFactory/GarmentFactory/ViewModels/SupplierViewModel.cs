﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GarmentFactory.ViewModels
{
    public class SupplierViewModel : ViewModelBase
    {
        private string _name;
        private string _description;
        private bool _isActive;
        private DateTime _lastSupply;


        public string Name { 
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }
        public string Description { 
            get => _description;
            set
            {
                _description = value;
                OnPropertyChanged("Description");
            }
        }
        public bool IsActive { 
            get => _isActive;
            set
            {
                _isActive = value;
                OnPropertyChanged("IsActive");
            }
        }
        public DateTime LastSupply { 
            get => _lastSupply;
            set
            {
                _lastSupply = value;
                OnPropertyChanged("LastSupply");
            }
        }
    }
}
