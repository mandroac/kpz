﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Liubomyr.Holovatyi.RobotChallenge
{
    public class LiubomyrHolovatyiAlgorithm : IRobotAlgorithm
    {
        private const int collectRange = 2;

        private Map copyMap;
        private List<Robot.Common.Robot> allRobots;
        private List<Robot.Common.Robot> myRobots;

        public string Author => "Liubomyr Holovatyi";

        Dictionary<Position, ModifiedEnergyStation> modifiedEnergyStations = new Dictionary<Position, ModifiedEnergyStation>();

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            copyMap = map;
            allRobots = (List<Robot.Common.Robot>)robots;
            myRobots = findMyRobots();

            var myRobot = robots[robotToMoveIndex];
            List<EnergyStation> availableStations = map.GetNearbyResources(myRobot.Position, collectRange); // станції, з яких робот вже може збирати енергію

            if (myRobot.Energy > 300 && myRobots.Count < 100) return new CreateNewRobotCommand() { NewRobotEnergy = 100 };

            var closeRobots = findClosestRobots(myRobot.Position, collectRange);


            if (availableStations.Count > 1) // якщо доступних станцій > 1
            {
                int availableEnergy = 0;

                foreach (var station in availableStations) availableEnergy += station.Energy;
                if (availableEnergy > 0)
                    return new CollectEnergyCommand();
                else return singleRobotMove(myRobot);
            }

            if (availableStations.Count == 1)
            {
                if (availableStations[0].Energy > 50) return new CollectEnergyCommand();
                else
                {
                    var availableStationNeighbors = map.GetNearbyResources(availableStations[0].Position, collectRange);

                    if (availableStationNeighbors.Count > 1)
                    {
                        var moveTo = new Position();
                        moveTo = closestPositionToStation(myRobot, GetModifiedStation(availableStations[0]).bestNeighbor);
                        return new MoveCommand() { NewPosition = moveTo };
                    }

                    else if (availableStations[0].Energy >= 20) return new CollectEnergyCommand();
                    else return singleRobotMove(myRobot);
                }

            }

            if (closeRobots.Count > 1)
            { 
                closeRobots.Remove(myRobot);
                closeRobots.Sort((r1,r2) => r1.Energy.CompareTo(r2.Energy));
                var tempCloseRobots = new List<Robot.Common.Robot>(closeRobots);

                foreach (var robot in closeRobots)
                {
                    if (robot.OwnerName == Author)
                    {
                        int iTempRobot = tempCloseRobots.IndexOf(robot);
                        tempCloseRobots.RemoveAt(iTempRobot);
                        tempCloseRobots.Insert(0,robot);
                    }
                }
                closeRobots = tempCloseRobots;
                bool isAttack = false;
                return actionToCloseRobot(myRobot, closeRobots, 0, out isAttack);
            
            }

            return singleRobotMove(myRobot);

        }

        private RobotCommand actionToCloseRobot(Robot.Common.Robot myRobot, List<Robot.Common.Robot> closeRobots, int currentCloseRobot, out bool isAttack)
        {
            RobotCommand returnCommand = new MoveCommand();
            isAttack = false;

            if (closeRobots[currentCloseRobot].OwnerName != myRobot.OwnerName)
            {
                int distanceEnergy = CalculateDistanceEnergy(myRobot.Position, closeRobots[currentCloseRobot].Position);
                int availableEnemyEnergy = (int)(closeRobots[currentCloseRobot].Energy * 0.1);

                if ((availableEnemyEnergy - distanceEnergy) > 30 && (distanceEnergy < myRobot.Energy))
                {
                    isAttack = true;
                    returnCommand = new MoveCommand() { NewPosition = closeRobots[currentCloseRobot].Position };
                }
            }
            if (currentCloseRobot < closeRobots.Count - 1) returnCommand = actionToCloseRobot(myRobot, closeRobots, ++currentCloseRobot, out isAttack);

            if (!isAttack) returnCommand = singleRobotMove(myRobot); //movementToCloseRobot(myRobot, closeRobots[currentCloseRobot]);

            return returnCommand;
        }

        private RobotCommand singleRobotMove(Robot.Common.Robot myRobot)
        {
            Random rnd = new Random();
            var moveTo = new Position(myRobot.Position.X, myRobot.Position.Y);

            int tempValue = rnd.Next(-3, 4);
            moveTo.X += tempValue;
            
            tempValue =  rnd.Next(-3, 4);
            moveTo.Y += tempValue;
            
            //while (!copyMap.IsValid(moveTo) || CalculateDistanceEnergy(moveTo, myRobot.Position) >= myRobot.Energy);
            
            return new MoveCommand() { NewPosition = moveTo };
        }

        private List<Robot.Common.Robot> findClosestRobots(Position position, int distance)
        {
            return allRobots.Where(robot => (Math.Abs(robot.Position.X - position.X) <= distance)
                && (Math.Abs(robot.Position.Y - position.Y) <= distance)).ToList();
        }

        private List<Robot.Common.Robot> findMyRobots()
        {
            return allRobots.Where(robot => robot.OwnerName == Author).ToList();
        }

        private Position closestPositionToStation(Robot.Common.Robot robot, ModifiedEnergyStation station)
        {
            Position pos = robot.Position;

            if (robot.Position.X < station.energyStation.Position.X - 2)
                pos.X += station.energyStation.Position.X - 2 - robot.Position.X;

            else if (robot.Position.X > station.energyStation.Position.X + 2)
                pos.X -= station.energyStation.Position.X - 2 - robot.Position.X;

            if (robot.Position.Y < station.energyStation.Position.Y - 2)
                pos.Y += station.energyStation.Position.Y - 2 - robot.Position.Y;

            else if (robot.Position.Y > station.energyStation.Position.Y + 2)
                pos.Y -= station.energyStation.Position.Y - 2 - robot.Position.Y;

            return pos;
        }

        private Position closestPositionToStation(Robot.Common.Robot robot, EnergyStation station)
        {
            Position pos = robot.Position;

            if (robot.Position.X < station.Position.X - 2)
                pos.X += (station.Position.X - 2) - robot.Position.X;

            else if (robot.Position.X > station.Position.X + 2)
                pos.X -= robot.Position.X - (station.Position.X + 2);

            if (robot.Position.Y < station.Position.Y - 2)
                pos.Y += (station.Position.Y - 2) - robot.Position.Y;

            else if (robot.Position.Y > station.Position.Y + 2)
                pos.Y -= robot.Position.Y - (station.Position.Y + 2);

            return pos;
        }

        private ModifiedEnergyStation GetModifiedStation(EnergyStation energyStation)
        {
            ModifiedEnergyStation returnStation;
            if(!modifiedEnergyStations.TryGetValue(energyStation.Position, out returnStation))
            {
                var neighborStations = copyMap.GetNearbyResources(energyStation.Position, collectRange);
                neighborStations.Remove(energyStation);
                modifiedEnergyStations[energyStation.Position] = new ModifiedEnergyStation(energyStation, neighborStations);
                returnStation = modifiedEnergyStations[energyStation.Position];
            }
            return returnStation;
        }

        public int CalculateDistanceEnergy(Position p1, Position p2)
        {
            return (p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y);
        }

    }
}
