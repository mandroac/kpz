﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Liubomyr.Holovatyi.RobotChallenge
{
    class ModifiedEnergyStation
    {
        public EnergyStation energyStation;

        public int neighbors;

        public List<EnergyStation> neighborStations;

        public EnergyStation bestNeighbor { get; set; }

        private const int collectionRange = 2;

        public ModifiedEnergyStation(EnergyStation _modifiedEnergyStation, List<EnergyStation> _neighborStations)
        {
            this.energyStation = _modifiedEnergyStation;
            this.neighborStations = _neighborStations;
            this.neighbors = _neighborStations.Count;
            this.bestNeighbor = findBestNeighbor();
        }

        public Boolean hasNeighbors()
        {
            return neighbors > 0 ? true : false;
        }

        private EnergyStation findBestNeighbor()
        {
            return neighborStations.Max();
        }
        
        public int maxCollectionEnergy()
        {
            if (neighbors == 0) return this.energyStation.Energy;
            else {
                int maxEnergy = this.energyStation.Energy;
                int tempMaxEnergy;
                foreach (var tempNeighbor in neighborStations)
                {
                    tempMaxEnergy = this.energyStation.Energy + tempNeighbor.Energy;
                    if (tempMaxEnergy > maxEnergy)
                    {
                        maxEnergy = tempMaxEnergy;
                        bestNeighbor = tempNeighbor;
                    }
                }
                return maxEnergy;
            }
        }


        public static bool operator >(ModifiedEnergyStation s1, ModifiedEnergyStation s2)
        {
            return s1.energyStation.Energy > s2.energyStation.Energy;
        }

        public static bool operator <(ModifiedEnergyStation s1, ModifiedEnergyStation s2)
        {
            return s1.energyStation.Energy < s2.energyStation.Energy;
        }

        public static bool operator >(ModifiedEnergyStation s1, int s2)
        {
            return s1.energyStation.Energy > s2;
        }

        public static bool operator <(ModifiedEnergyStation s1, int s2)
        {
            return s1.energyStation.Energy < s2;
        }
    }
}
