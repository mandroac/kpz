﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using Liubomyr.Holovatyi.RobotChallenge;

namespace Holovatyi.Test
{
    [TestClass]
    public class UnitTest1
    {

        // CreateNewRobotCommand
        // CollectEnergyCommand
        // MoveCommand

        [TestMethod]
        public void AuthorChecker()
        {
            LiubomyrHolovatyiAlgorithm liubomyrHolovatyiAlgorithm = new LiubomyrHolovatyiAlgorithm();
            Map map = new Map();
            Robot.Common.Robot robot = new Robot.Common.Robot()
            {
                Position = new Position(1, 5),
                Energy = 200,
                OwnerName = "Liubomyr Holovatyi"
            };

            Robot.Common.Robot robot2 = new Robot.Common.Robot()
            {
                Position = new Position(1, 4),
                Energy = 200,
                OwnerName = "not Liubomyr Holovatyi"
            };

            Assert.IsTrue(liubomyrHolovatyiAlgorithm.Author == robot.OwnerName);
            Assert.IsFalse(liubomyrHolovatyiAlgorithm.Author == robot2.OwnerName);

        }

        [TestMethod]
        public void MoveBetweenTwoStationsTest()
        {
            LiubomyrHolovatyiAlgorithm liubomyrHolovatyiAlgorithm = new LiubomyrHolovatyiAlgorithm();
            Map map = new Map();
            
            Robot.Common.Robot robot = new Robot.Common.Robot() {
                Position = new Position(1,5),
                Energy = 200,
                OwnerName = "Liubomyr Holovatyi"
            };

            var robots = new List<Robot.Common.Robot>();
            robots.Add(robot);
            map.Stations.Add(new EnergyStation() { Position = new Position(1, 3), Energy = 30 });
            map.Stations.Add(new EnergyStation() { Position = new Position(2, 2), Energy = 60 });

            Assert.IsTrue(liubomyrHolovatyiAlgorithm.DoStep(robots, 0, map) is MoveCommand);
        }


        [TestMethod]
        public void SeveralCloseAllyRobotsActions()
        {
            LiubomyrHolovatyiAlgorithm liubomyrHolovatyiAlgorithm = new LiubomyrHolovatyiAlgorithm();
            Map map = new Map() { MaxPozition = new Position(100,100)};
            var robots = new List<Robot.Common.Robot>();

            robots.Add(new Robot.Common.Robot() { Energy = 150, Position = new Position(31, 15), OwnerName = "Liubomyr Holovatyi" });
            robots.Add(new Robot.Common.Robot() { Energy = 150, Position = new Position(30, 16), OwnerName = "Liubomyr Holovatyi" });
            robots.Add(new Robot.Common.Robot() { Energy = 200, Position = new Position(32, 17), OwnerName = "Liubomyr Holovatyi" });

            Assert.IsInstanceOfType(liubomyrHolovatyiAlgorithm.DoStep(robots, 0, map), new MoveCommand().GetType());
        }

        [TestMethod]
        public void SeveralCloseEnemyAndAllyRobotsActions()
        {
            LiubomyrHolovatyiAlgorithm liubomyrHolovatyiAlgorithm = new LiubomyrHolovatyiAlgorithm();
            Map map = new Map() { MaxPozition = new Position(100, 100) };
            var robots = new List<Robot.Common.Robot>();

            robots.Add(new Robot.Common.Robot() { Energy = 150, Position = new Position(31, 15), OwnerName = "Liubomyr Holovatyi" });
            robots.Add(new Robot.Common.Robot() { Energy = 900, Position = new Position(30, 16), OwnerName = "Donald Trump" });
            robots.Add(new Robot.Common.Robot() { Energy = 15000, Position = new Position(30, 15), OwnerName = "Liubomyr Holovatyi" });
            robots.Add(new Robot.Common.Robot() { Energy = 1000, Position = new Position(32, 17), OwnerName = "Donald Trump" });

            Assert.AreEqual((liubomyrHolovatyiAlgorithm.DoStep(robots, 0, map) as MoveCommand).NewPosition, new Position(32, 17));
        }

        [TestMethod]
        public void EmptyStationMoveTest()
        {
            LiubomyrHolovatyiAlgorithm liubomyrHolovatyiAlgorithm = new LiubomyrHolovatyiAlgorithm();
            Map map = new Map() { MaxPozition = new Position(100, 100) };
            var robots = new List<Robot.Common.Robot>();

            robots.Add(new Robot.Common.Robot() { Energy = 150, Position = new Position(10, 15), OwnerName = "Liubomyr Holovatyi" });
            robots.Add(new Robot.Common.Robot() { Energy = 150, Position = new Position(8, 16), OwnerName = "Liubomyr Holovatyi" });
            map.Stations.Add(new EnergyStation() { Position = new Position(9, 14), Energy = 0 });

            Assert.IsTrue(liubomyrHolovatyiAlgorithm.DoStep(robots, 0, map) is MoveCommand);

        }


        [TestMethod]
        public void CollectEnergyTest()
        {
            LiubomyrHolovatyiAlgorithm liubomyrHolovatyiAlgorithm = new LiubomyrHolovatyiAlgorithm();
            Map map = new Map();

            Robot.Common.Robot robot = new Robot.Common.Robot()
            {
                Position = new Position(17, 5),
                Energy = 200,
                OwnerName = "Liubomyr Holovatyi"
            };

            var robots = new List<Robot.Common.Robot>();
            robots.Add(robot);
            map.Stations.Add(new EnergyStation() { Position = new Position(16, 7), Energy = 70 });
            

            Assert.IsTrue(liubomyrHolovatyiAlgorithm.DoStep(robots, 0, map) is CollectEnergyCommand);

        }

        [TestMethod]
        public void OneAvailableStationWithNeighborTest()
        {
            LiubomyrHolovatyiAlgorithm liubomyrHolovatyiAlgorithm = new LiubomyrHolovatyiAlgorithm();
            Map map = new Map();

            Robot.Common.Robot robot = new Robot.Common.Robot()
            {
                Position = new Position(20, 20),
                Energy = 200,
                OwnerName = "Liubomyr Holovatyi"
            };

            var robots = new List<Robot.Common.Robot>();
            robots.Add(robot);
            map.Stations.Add(new EnergyStation() { Position = new Position(22, 19), Energy = 30 });
            map.Stations.Add(new EnergyStation() { Position = new Position(24, 19), Energy = 500 });



            Assert.IsInstanceOfType(liubomyrHolovatyiAlgorithm.DoStep(robots, 0, map), typeof(MoveCommand));

        }

        [TestMethod]
        public void SeveralEmptyStationsMoveTest()
        {
            LiubomyrHolovatyiAlgorithm liubomyrHolovatyiAlgorithm = new LiubomyrHolovatyiAlgorithm();
            Map map = new Map();

            Robot.Common.Robot robot = new Robot.Common.Robot()
            {
                Position = new Position(20, 20),
                Energy = 200,
                OwnerName = "Liubomyr Holovatyi"
            };

            var robots = new List<Robot.Common.Robot>();
            robots.Add(robot);
            map.Stations.Add(new EnergyStation() { Position = new Position(22, 19), Energy = 0 });
            map.Stations.Add(new EnergyStation() { Position = new Position(21, 19), Energy = 0 });
            map.Stations.Add(new EnergyStation() { Position = new Position(19, 19), Energy = 0 });

            Assert.IsInstanceOfType(liubomyrHolovatyiAlgorithm.DoStep(robots, 0, map), typeof(MoveCommand));

        }

    }
}
