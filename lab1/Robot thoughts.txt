Станцій у 20 рази більше ніж усіх роботів на початку змагання, кожна з яких генерує від
20 до 40 одиниць енергії за хід. При нападі на іншого робота, забирається 10% його
енергії і втрачається 30 одиниць енергії Максимальна енергія станції 5000. Збір з станції
на відстані 2 клітинок.

_________________________________________________________________________

MaxEnergyGrowth = 40;
MinEnergyGrowth = 20;
MaxStationEnergy = 5000;
CollectingDistance = 2;
MaxEnergyCanCollect = 300;

EnergyStationForAttendant = 20;
EnergyLossToCreateNewRobot = 100;

StoleRateEnergyAtAttack = 0.1;
AttackEnergyLoss = 30;

_________________________________________________________________________

13 студентів
130 роботів на початку
260 станцій

_________________________________________________________________________

Площа станцій: 25 * 260 = 6 500
площа поля 10 000
Шанс що на початку гри робот буде в зоні дії 1 станції: 0,65
Шанс що на початку гри робот буде в зоні дії 2 станцій: 


_________________________________________________________________________

1. Чи можна наступати на станцію?
2. Що буде, якщо робот розрядиться без станції поруч?

_________________________________________________________________________

ModifiedEnergyStation bestStation;

                    foreach (var tempStation in availableStations)
                    {
                        List<EnergyStation> tempAvailableStations = map.GetNearbyResources(tempStation.Position, collectRange); // шукаємо скільки сусідів в доступних станцій
                        modifiedEnergyStations[tempStation.Position] = new ModifiedEnergyStation(tempStation, tempAvailableStations); // модифікуємо достпну станцію
                    }

                    bestStation = GetModifiedStation(findClosestAvailableStation(myRobot, availableStations));
                    List<Position> positions = bestStation.commonBestNeighborPositions();

                    // якщо ми стоїмо посеред декількох станцій - збираємо енергію
                    // в іншому випадку йдемо у точку між станціями

                    if (positions.Contains(myRobot.Position)) return new CollectEnergyCommand();
                    else return new MoveCommand() { NewPosition = positions[0] }; // поки що берему першу зі спільних позицій між станціями       


        private EnergyStation findClosestAvailableStation(Robot.Common.Robot robot, List<EnergyStation> availableStations)
        {
            List<ModifiedEnergyStation> modifiedEnergyStations = new List<ModifiedEnergyStation>();
            foreach (var station in availableStations) modifiedEnergyStations.Add(GetModifiedStation(station));

            Position moveTo = robot.Position;
            EnergyStation returnStation = availableStations[0];
            int energyRequired = 100000000;

            foreach (var station in modifiedEnergyStations)
            {
                moveTo = closestPositionToStation(robot, station);

                if (CalculateDistanceEnergy(robot.Position, moveTo) < energyRequired )
                {
                    energyRequired = CalculateDistanceEnergy(robot.Position, moveTo);
                    returnStation = station.energyStation;
                }
            }

            return returnStation;
        }         

_________________________________________________________________________


private RobotCommand actionToCloseRobot(Robot.Common.Robot myRobot, List<Robot.Common.Robot> closeRobots, int currentCloseRobot, out bool isAttack)
        {
            RobotCommand returnCommand = new MoveCommand();
            isAttack = false;

            if (closeRobots[currentCloseRobot].OwnerName != myRobot.OwnerName)
            {
                int distanceEnergy = CalculateDistanceEnergy(myRobot.Position, closeRobots[currentCloseRobot].Position);
                int availableEnemyEnergy = (int)(closeRobots[currentCloseRobot].Energy * 0.1);

                if ((availableEnemyEnergy - distanceEnergy) > 30 && (distanceEnergy < myRobot.Energy))
                {
                    isAttack = true;
                    returnCommand = new MoveCommand() { NewPosition = closeRobots[currentCloseRobot].Position };
                }
            }
            if (currentCloseRobot < closeRobots.Count - 1) returnCommand = actionToCloseRobot(myRobot, closeRobots, ++currentCloseRobot, out isAttack);
            
            if (!isAttack)  returnCommand = movementToCloseRobot(myRobot, closeRobots[currentCloseRobot]);

            return returnCommand;
        }


        private Position closestPositionToStation(Robot.Common.Robot robot, EnergyStation station)
        {
            Position pos = robot.Position;

            if (robot.Position.X < station.Position.X - 2)
                pos.X += (station.Position.X - 2) - robot.Position.X;

            else if (robot.Position.X > station.Position.X + 2)
                pos.X -= robot.Position.X - (station.Position.X + 2);

            if (robot.Position.Y < station.Position.Y - 2)
                pos.Y += (station.Position.Y - 2) - robot.Position.Y;

            else if (robot.Position.Y > station.Position.Y + 2)
                pos.Y -= robot.Position.Y - (station.Position.Y + 2);

            return pos;
        }


_________________________________________________________________________

        public List<Position> commonBestNeighborPositions()
        {
            /*if (bestNeighbor is null)*/ _ = maxCollectionEnergy();
            List<Position> positions = new List<Position>();
            int startX = this.energyStation.Position.X - 2;
            int startY = this.energyStation.Position.Y - 2;
            Position startPosition = new Position(startX, startY);
            for (int i = 0; i <= collectionRange*2; i++)
            {
                for (int j = 0; j <= collectionRange*2; j++)
                {
                    if ((Math.Abs((startPosition.X + i) - bestNeighbor.Position.X) <= collectionRange)
                        && (Math.Abs((startPosition.Y + j) - bestNeighbor.Position.Y) <= collectionRange))
                        positions.Add(new Position(startPosition.X + i, startPosition.Y + j));
                }
            }
            return positions; 
        }

_________________________________________________________________________

        private RobotCommand movementToCloseRobot(Robot.Common.Robot myRobot, Robot.Common.Robot anotherRobot)
        {
            int maxX = copyMap.MaxPozition.X;
            int maxY = copyMap.MaxPozition.Y;
            Position moveTo = new Position(myRobot.Position.X, myRobot.Position.Y);

            // Чверті:
            // 1 2
            // 3 4

            if (myRobot.Position.X <= maxX / 2 && myRobot.Position.Y <= maxY / 2) // 1 чверть
            {
                if (myRobot.Position.X >= anotherRobot.Position.X) moveTo.X += 2;
                else moveTo.Y += 2;
            }

            else if (myRobot.Position.X > maxX / 2 && myRobot.Position.Y <= maxY / 2) // 2 чверть
            {
                if (myRobot.Position.X <= anotherRobot.Position.X) moveTo.X -= 2;
                else moveTo.Y += 2;
            }

            else if (myRobot.Position.X <= maxX / 2 && myRobot.Position.Y > maxY / 2) // 3 чверть
            {
                if (myRobot.Position.X >= anotherRobot.Position.X) moveTo.X += 2;
                else moveTo.Y -= 2;
            }

            else if (myRobot.Position.X > maxX / 2 && myRobot.Position.Y > maxY / 2) // 4 чверть
            {
                if (myRobot.Position.X <= anotherRobot.Position.X) moveTo.X -= 2;
                else moveTo.Y -= 2;
            }

            else // точка [maxX/2][maxY/2]
            {
                moveTo.X++;
                moveTo.Y++;
            }


            if (myRobots.Any(r => r.Position == moveTo))
                moveTo = copyMap.FindFreeCell(moveTo, myRobots);


            return new MoveCommand() { NewPosition = moveTo };

        }
